import 'dart:ui';
import 'dart:js' as js;

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Directionality(
        textDirection: TextDirection.ltr,
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/isoroom.png'), fit: BoxFit.cover)),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  color: Colors.black.withOpacity(0.6),
                  padding: EdgeInsets.all(25.0),
                  child: Text(
                    'sibben.dev',
                    textDirection: TextDirection.ltr,
                    style: TextStyle(fontSize: 30, color: Colors.white),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  padding: EdgeInsets.all(7),
                  color: Colors.black.withOpacity(.6),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Center(
                          child: IconButton(
                        icon: FaIcon(FontAwesomeIcons.linkedin,
                            color: Colors.white),
                        onPressed: () {
                          js.context.callMethod('open', [
                            'https://linkedin.com/in/cally-sibben-430017134'
                          ]);
                        },
                      )),
                      Center(
                        child: IconButton(
                          icon: FaIcon(
                            FontAwesomeIcons.envelopeOpen,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            js.context.callMethod(
                                'open', ['mailto:cally@sibben.dev']);
                          },
                        ),
                      ),
                      Center(
                        child: IconButton(
                          icon: FaIcon(
                            FontAwesomeIcons.gitlab,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            js.context.callMethod(
                                'open', ['https://gitlab.com/yocally']);
                          },
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
